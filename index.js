const mysql = require('mysql');
const express = require('express');
var app = express();
const bodyparser = require('body-parser');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyparser.json());
var mysqlConnection = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'ayse',
    database:'bisu'

});

mysqlConnection.connect((err)=>{
    if(!err)
    console.log('DB connection succeed.');
    else
    console.log('DB connection failed \n'+ JSON.stringify(err, undefined, 2));
});

app.listen(3000,()=>console.log('Express server is running at port no:3000'));

app.get('/api/getCustomerInfo',(req,res)=>{
    mysqlConnection.query('SELECT * FROM Subscription ',(err, result)=>{
        if(!err)
        res.send({data:result});
        else
        console.log(err);
        
    })
});

app.get('/api/getCustomerInfo/:phoneNumber',(req,res)=>{
    mysqlConnection.query('SELECT * FROM Subscription WHERE phoneNumber=?',[req.params.phoneNumber],(err, result)=>{
        if(!err)
        res.send({data:result});
        else
        console.log(err);
        
    })
});

app.post('/api/getSubscriptionOrders/:subscriptionId',(req,res)=>{
    mysqlConnection.query('SELECT * FROM Orders WHERE subscriptionId = ?',[req.params.subscriptionId],(err, result)=>{
        if(!err)
        res.send({data:result});
        else
        console.log(err);
        
    })
});

app.post('/api/getSubscriptionOrders',(req,res)=>{
    mysqlConnection.query('SELECT * FROM Orders',(err, result)=>{
        if(!err)
        res.send({data:result});
        else
        console.log(err);
        
    })
});





