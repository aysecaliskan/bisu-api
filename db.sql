CREATE DATABASE bisu;
use bisu;
CREATE TABLE bisu.Subscription (
    subscriptionId VARCHAR(6) PRIMARY KEY,
    fullname VARCHAR(100) NOT NULL,
    address VARCHAR(150) NOT NULL,
    locationName VARCHAR(100),
    subCityName VARCHAR(100),
    cityName VARCHAR(100),
    brand VARCHAR(100),
    phoneNumber VARCHAR(50),
    distributorNumber VARCHAR(50)
);

INSERT INTO Subscription (
    subscriptionId,
    fullname,
    address,
    locationName,
    subCityName,
    cityName,
    brand,
    phoneNumber,
    distributorNumber
  )
VALUES (
    'abc126',
    'Ergin',
    'moda caddesi',
    'göztepe',
    'kadiköy',
    'istanbul',
    'sirma',
    '5332858530',
    '2161000002'
  );
  
CREATE TABLE bisu.Orders (
    orderId MEDIUMINT(100)  NOT NULL AUTO_INCREMENT,
    subscriptionId VARCHAR(6),
    FOREIGN KEY (subscriptionId)
    REFERENCES subscription (subscriptionId),
    deliveryDate DATETIME ,
    paymentMethod VARCHAR(50),
    products VARCHAR(150),
    totalAmount DECIMAL(8,2),
    status VARCHAR(50),
	PRIMARY KEY (orderId)
);

INSERT INTO orders (
	subscriptionId,
    deliveryDate,
    paymentMethod,
    products,
    totalAmount,
    status
  )
VALUES (
    'abc126',
    '2017-03-09 19:02',
    ' MASTERPASS',
    '[{"product":"19 lt damanaca", "quantity":1 }, {"product":"5 lt pet", "quantity":3 }]',
    '32.50',
    'CANCELED'
  );
